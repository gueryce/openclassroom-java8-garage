package tp.openclassroom.garage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import tp.openclassroom.contantes.Marque;
import tp.openclassroom.vehicule.Vehicule;

public class Garage {
	
	private List<Vehicule> voitures = new ArrayList<Vehicule>();
	private ObjectOutputStream objectOutputStream;
	private ObjectInputStream objectInputStream;

	
	public Garage() {
		charger();
	}
	
	public void addVoiture(Vehicule voit) {
		voitures.add(voit);
	}

	public void sauvegarde() {
		try {
			if(!voitures.isEmpty()) {
				File file = new File("garage.ser");
				if(!file.exists()) {
					FileOutputStream fileOutputStream = new FileOutputStream(file);
					DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(fileOutputStream));
					objectOutputStream = new ObjectOutputStream(dataOutputStream);
					objectOutputStream.writeObject(voitures);
					objectOutputStream.close();
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void charger() {
		try {
			File file = new File("garage.ser");
			if(file.exists()) {
				FileInputStream fis = new FileInputStream(file);
				DataInputStream dis = new DataInputStream(new BufferedInputStream(fis));
				objectInputStream = new ObjectInputStream(dis);		
				
				Object readObject = objectInputStream.readObject();
				if(readObject instanceof List<?>) {
					voitures = (List<Vehicule>) readObject;
				}
			}					
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	// *************************** Supplement Java 8 ************************/
	public void selectionneVehiculeSelonPrix(List<Vehicule> voit, Predicate<Vehicule> tester,
			Function<Vehicule, String> mapper, Consumer<String> block) {
		for (Vehicule vehicule : voit) {
			if (tester.test(vehicule)) {
				String mess = mapper.apply(vehicule);
				block.accept(mess);
			}
		}
	}
	
	public void testJava8() {
		//Add-on
		selectionneVehiculeSelonPrix(voitures,
				v -> v.getMarque() == Marque.PIGEOT,
				v -> v.getMarque().toString(), 
				mess -> System.out.println("Marque trouv�e: "+ mess));
	}

	//************************************************************************/
	
	private void afficheListe() {
		Stream<Vehicule> stream = voitures.stream();
		Stream<String> map = stream.map(x-> x.toString());
		map.forEach(System.out::println);
	}
	
	@Override
	public String toString() {
		String info = "";
		System.out.println("*************************");
		System.out.println("* Garage OpenClassRooms *");
		System.out.println("*************************");
		afficheListe();
		//voitures.stream().filter(Optional::isPresent).map(Object::toString).collect(Collectors.joining("\n"));
		return info;
	}
}
