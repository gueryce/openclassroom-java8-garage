package tp.openclassroom.vehicule;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import tp.openclassroom.contantes.Marque;
import tp.openclassroom.moteur.Moteur;
import tp.openclassroom.options.Option;

public class Vehicule implements Serializable {
	
	static NumberFormat currencyInstance = NumberFormat.getCurrencyInstance(Locale.FRANCE);

	protected String nom;
	protected Marque nomMarque;
	protected Double prix;
	private List<Option> options = new ArrayList<Option>();
	private Moteur moteur;
	
	public void addOption(Option opt) {
		options.add(opt);
	}
	
	public Marque getMarque() {
		return nomMarque;
	}
	
	
	public List<Option> getOptions(){
	return options;
	}
	
	
	public Double getPrix() {
		return prix;
		
	}

	public void setMoteur(Moteur moteur) {
		this.moteur = moteur;
	}
	
	@Override
	public String toString() {
		return "+ Voiture " + getMarque() + ":" + getClass().getSimpleName() + " " + moteur.toString() + " ("
				+ options.stream().map(Object::toString).collect(Collectors.joining(",")) + ") d'une valeur totale de "
				+ getPrix() + currencyInstance.getCurrency().getSymbol();
	}
	
	
}
