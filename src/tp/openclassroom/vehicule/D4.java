package tp.openclassroom.vehicule;

import tp.openclassroom.contantes.Marque;

public class D4 extends Vehicule {

	public D4() {
		super();
		super.nom  = this.getClass().getName();
		super.nomMarque = Marque.TROEN;
		super.prix = 18000d;
	}

}
