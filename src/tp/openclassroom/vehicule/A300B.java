package tp.openclassroom.vehicule;

import tp.openclassroom.contantes.Marque;

public class A300B extends Vehicule {

	public A300B() {
		super();
		super.nom  = this.getClass().getName();
		super.nomMarque = Marque.PIGEOT;
		super.prix = 22000d;
	}

}
