package tp.openclassroom.moteur;

import tp.openclassroom.contantes.TypeMoteur;

public class MoteurHybride extends Moteur {

	public MoteurHybride(String cylindre, Double prix) {
		super(cylindre, prix);
		super.typeMoteur= TypeMoteur.HYBRIDE;
	}

}
