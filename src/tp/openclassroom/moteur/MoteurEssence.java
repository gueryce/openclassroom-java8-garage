package tp.openclassroom.moteur;

import tp.openclassroom.contantes.TypeMoteur;

public class MoteurEssence extends Moteur {

	
	
	
	public MoteurEssence(String cylindre, Double prix) {
		super(cylindre, prix);
		super.typeMoteur  = TypeMoteur.ESSENCE;
	}

}
