package tp.openclassroom.moteur;

import tp.openclassroom.contantes.TypeMoteur;

public class MoteurElectrique extends Moteur {

	public MoteurElectrique(String cylindre, Double prix) {
		super(cylindre, prix);
		super.typeMoteur = TypeMoteur.ELECTRIQUE;
	}

}
