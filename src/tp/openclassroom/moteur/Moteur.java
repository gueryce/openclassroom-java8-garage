package tp.openclassroom.moteur;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;

import tp.openclassroom.contantes.TypeMoteur;

public class Moteur implements Serializable {
	
	static NumberFormat currencyInstance = NumberFormat.getCurrencyInstance(Locale.FRANCE);
	

	protected TypeMoteur typeMoteur;
	private String cylindre;
	private Double prix;
	
	public Moteur(String cylindre, Double prix) {
		this.cylindre = cylindre;
		this.prix = prix;
	}

	@Override
	public String toString() {
		return " Moteur " + typeMoteur + " " + cylindre + " (" + prix + currencyInstance.getCurrency().getSymbol()+") ";
	}
	
	public Double getPrix() {
		Double prix = 0d;
		return prix;
	}
}
