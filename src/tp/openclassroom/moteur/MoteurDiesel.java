package tp.openclassroom.moteur;

import tp.openclassroom.contantes.TypeMoteur;

public class MoteurDiesel extends Moteur {

	public MoteurDiesel(String cylindre, Double prix) {
		super(cylindre, prix);
		super.typeMoteur = TypeMoteur.DIESEL;
	}

}
