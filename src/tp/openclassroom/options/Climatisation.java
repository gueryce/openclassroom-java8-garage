package tp.openclassroom.options;

public class Climatisation implements Option {

	@Override
	public Double getPrix() {
		double prix = 347.30d;
		return prix;
	}

	@Override
	public String toString() {
		return "Climatisation (" + getPrix() + currencyInstance.getCurrency().getSymbol()+ ")";
	}

	
}
