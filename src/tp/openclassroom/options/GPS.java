package tp.openclassroom.options;

public class GPS implements Option {

	@Override
	public Double getPrix() {
		double prix = 113.50d;
		return prix;
	}
	
	@Override
	public String toString() {
		return "GPS (" + getPrix() + currencyInstance.getCurrency().getSymbol()+ ")";
	}

}
