package tp.openclassroom.options;

public class SiegeChauffant implements Option {

	@Override
	public Double getPrix() {
		double prix = 562.90d;
		return prix;
	}
	
	@Override
	public String toString() {
		return "Siege chauffant (" + getPrix() + currencyInstance.getCurrency().getSymbol()+ ")";
	}

}
