package tp.openclassroom.options;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;

public interface Option extends Serializable {
	
	static NumberFormat currencyInstance = NumberFormat.getCurrencyInstance(Locale.FRANCE);
	
	Double getPrix();

}
