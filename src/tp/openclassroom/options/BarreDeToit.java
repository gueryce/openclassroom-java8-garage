package tp.openclassroom.options;

public class BarreDeToit implements Option {

	@Override
	public Double getPrix() {
		double prix = 200d;
		return prix;
	}

	@Override
	public String toString() {
		return "Barre de toit (" + getPrix()+ currencyInstance.getCurrency().getSymbol()+ ")";
	}
	
}
