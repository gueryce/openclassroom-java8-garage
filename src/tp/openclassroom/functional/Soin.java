package tp.openclassroom.functional;

@FunctionalInterface
public interface Soin {

	public String soigne(String str);
}
