package tp.openclassroom.contantes;

public enum TypeMoteur {
	DIESEL,
	ESSENCE,
	HYBRIDE,
	ELECTRIQUE

}
